package com.mgiorda.sdrproxyclient.impl.resttemplate;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import java.lang.annotation.Annotation;
import java.util.List;

@Data
@Builder
public class ClientConfig {

    @NonNull
    @Builder.Default
    private Boolean proxyRelationships = true;

    @NonNull
    @Builder.Default
    private Boolean  jsonErrorHandler = true;

}

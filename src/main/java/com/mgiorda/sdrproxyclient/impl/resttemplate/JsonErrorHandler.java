package com.mgiorda.sdrproxyclient.impl.resttemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.HttpClientErrorException;

import java.io.IOException;

public class JsonErrorHandler extends DefaultResponseErrorHandler {

    private final ObjectMapper jsonMapper;

    public JsonErrorHandler(@NonNull ObjectMapper jsonMapper) {
        this.jsonMapper = jsonMapper;
    }

    @Override
    public void handleError(ClientHttpResponse response) throws IOException {

        if(HttpStatus.NOT_FOUND == response.getStatusCode()) {
            super.handleError(response);
        }
        else {
            Object errorResponse = jsonMapper.readValue(response.getBody(), Object.class);

            throw new HttpClientErrorException(response.getStatusCode(), errorResponse.toString());
        }
    }
}
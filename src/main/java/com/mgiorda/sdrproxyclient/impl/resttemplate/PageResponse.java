package com.mgiorda.sdrproxyclient.impl.resttemplate;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class PageResponse {

    private Map<String, List<JsonNode>> _embedded;

    private Map<String, Link> _links;

    private Page page;

    @Data
    public static class Link {

        private String href;
        private Boolean templated;
    }

    @Data
    public static class Page {

        private Integer size;
        private Integer totalElements;
        private Integer totalPages;
        private Integer number;
    }
}

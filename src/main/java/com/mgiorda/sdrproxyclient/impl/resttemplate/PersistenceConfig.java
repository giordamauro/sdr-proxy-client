package com.mgiorda.sdrproxyclient.impl.resttemplate;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.lang.annotation.Annotation;
import java.util.List;

@Data
@Builder
public class PersistenceConfig {

    @NonNull
    private final Class<? extends Annotation> entityAnnType;

    @NonNull
    private final Class<? extends Annotation> relationshipAnnType;

    @NonNull
    private final List<Class<? extends Annotation>> idAnnTypes;
}

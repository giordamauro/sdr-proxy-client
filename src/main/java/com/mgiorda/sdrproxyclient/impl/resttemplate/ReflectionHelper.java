package com.mgiorda.sdrproxyclient.impl.resttemplate;

import lombok.NonNull;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

class ReflectionHelper {

    public Object getFieldValue(@NonNull Object target, @NonNull String fieldName) {

        try {
            Field field = target.getClass().getDeclaredField(fieldName);

            return this.getFieldValue(target, field);

        } catch (Exception e) {
            throw new IllegalStateException("Exception getting field value", e);
        }
    }

    public Object getFieldValue(@NonNull Object object, @NonNull Field field) {

        try {
            field.setAccessible(true);

            return field.get(object);

        } catch (Exception e) {
            throw new IllegalStateException("Exception getting field value", e);
        }
    }

    public void setFieldValue(@NonNull Object target, @NonNull Field field, Object value) {

        try {
            field.setAccessible(true);
            field.set(target, value);

        } catch (Exception e) {

            throw new IllegalStateException("Exception setting field value", e);
        }
    }

    public Field findSingleAnnotatedField(@NonNull Class<?> entityClass, @NonNull List<Class<? extends Annotation>> annTypes) {

        for (Class<? extends Annotation> annType : annTypes) {

            Set<Field> idFields = this.getAnnotatedFields(entityClass, annType);

            if (idFields.size() > 1) {
                throw new IllegalStateException("Unsupported multiple Id fields");
            }

            if (idFields.size() == 1) {
                return idFields.iterator().next();
            }
        }

        throw new IllegalStateException(String.format("Entity ID not found for class '%s' - Id types: %s", entityClass, annTypes));
    }

    public Set<Field> getAnnotatedFields(@NonNull Class clazz, @NonNull Class<? extends Annotation> annotationClass) {

        Set<Field> annotatedFields = new HashSet<>();

        for (Field field : getDeclaredFields(clazz)) {

            if (field.isAnnotationPresent(annotationClass)) {
                annotatedFields.add(field);
            }
        }

        return annotatedFields;
    }

    public Set<Field> getDeclaredFields(@NonNull Class clazz) {

        Set<Field> fields = new HashSet<>();

        Field[] declaredFields = clazz.getDeclaredFields();
        Collections.addAll(fields, declaredFields);

        Class superClass = clazz.getSuperclass();

        if (superClass != null) {
            Set<Field> declaredFieldsOfSuper = getDeclaredFields(superClass);
            fields.addAll(declaredFieldsOfSuper);
        }

        return fields;
    }

    public Method getGetterMethod(@NonNull Class<?> entityType, @NonNull String fieldName) {

        try {
            String methodName = "get" + Character.toUpperCase(fieldName.charAt(0)) + fieldName.substring(1);

            return entityType.getDeclaredMethod(methodName);

        } catch (Exception e) {
            throw new IllegalStateException(String.format("Exception getting Getter method for field '%s' in class %s", fieldName, entityType), e);
        }
    }

    public PropertyDescriptor getPropertyDescriptor(@NonNull Class<?> entityType, @NonNull String fieldName) {

        try {
            return  new PropertyDescriptor(fieldName, entityType);

        } catch (IntrospectionException e) {
            throw new IllegalStateException(String.format("Exception getting PropertyDescriptor for field '%s' in class %s", fieldName, entityType), e);
        }
    }

    public void makeDefaultConstructorAccessible(@NonNull Class<?> entityType) {

        try {
            Constructor<?> constructor = entityType.getDeclaredConstructor();
            constructor.setAccessible(true);

        } catch (NoSuchMethodException e) {
            throw new IllegalStateException("Exception making default constructor accessible for " + entityType, e);
        }
    }

}

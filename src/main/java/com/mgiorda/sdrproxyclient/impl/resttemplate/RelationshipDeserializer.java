package com.mgiorda.sdrproxyclient.impl.resttemplate;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.deser.BeanDeserializerModifier;
import com.mgiorda.sdrproxyclient.support.RepositoryClientFactory;
import com.mgiorda.sdrproxyclient.model.HttpRequest;
import com.mgiorda.sdrproxyclient.model.RestClient;
import lombok.NonNull;
import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RelationshipDeserializer extends JsonDeserializer<Object> {

    private final Class<?> entityType;

    private final ObjectMapper jsonMapper;
    private final PersistenceConfig persistenceConfig;
    private final ReflectionHelper reflectionHelper;

    private final RestClient restClient;

    RelationshipDeserializer(@NonNull ObjectMapper defaultJsonMapper, @NonNull Class<?> entityType, @NonNull RestClient restClient, @NonNull PersistenceConfig persistenceConfig) {

        this.reflectionHelper = new ReflectionHelper();

        this.jsonMapper = defaultJsonMapper;
        this.entityType = entityType;
        this.persistenceConfig = persistenceConfig;
        this.restClient = restClient;
    }

    @Override
    public Object deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {

        Object result = jsonMapper.readValue(jp, entityType);

        if (!entityType.isAnnotationPresent(persistenceConfig.getEntityAnnType())) {
            return result;
        }

        Map<Method, HttpRequest> relationshipMethods = null;

        Field[] declaredFields = entityType.getDeclaredFields();
        for (Field field : declaredFields) {

            if (field.isAnnotationPresent(persistenceConfig.getRelationshipAnnType()) && !List.class.isAssignableFrom(field.getType())) {

                String fieldName = field.getName();

                Method getterMethod = reflectionHelper.getGetterMethod(entityType, fieldName);
                HttpRequest<Object> request = this.getRelationshipRequest(result, fieldName);

                if(relationshipMethods == null) {
                    relationshipMethods = new HashMap<>();
                }

                relationshipMethods.put(getterMethod, request);
            }
        }

        return (relationshipMethods == null) ? result : this.proxyEntity(result, relationshipMethods);
    }

    public static class Modifier extends BeanDeserializerModifier {

        private final RestClient restClient;
        private final PersistenceConfig persistenceConfig;
        private final ObjectMapper defaultJsonMapper;

        Modifier(@NonNull RestClient restClient, @NonNull PersistenceConfig persistenceConfig, @NonNull ObjectMapper defaultJsonMapper) {

            this.restClient = restClient;
            this.persistenceConfig = persistenceConfig;
            this.defaultJsonMapper = defaultJsonMapper;
        }

        @Override
        @SuppressWarnings("unchecked")
        public JsonDeserializer<?> modifyDeserializer(DeserializationConfig config, BeanDescription beanDesc, JsonDeserializer<?> deserializer) {
            return new RelationshipDeserializer(defaultJsonMapper, beanDesc.getBeanClass(), restClient, persistenceConfig);
        }
    }

    private HttpRequest<Object> getRelationshipRequest(@NonNull Object entity, @NonNull String fieldName) {

        Field idField = reflectionHelper.findSingleAnnotatedField(entity.getClass(), persistenceConfig.getIdAnnTypes());
        Object id = reflectionHelper.getFieldValue(entity, idField);

        String repositoryName = RepositoryClientFactory.getDefaultRepositoryPathFor(entity.getClass());

        return HttpRequest.builder()
                .httpMethod(HttpMethod.GET)
                .servicePath("/" + repositoryName + "/" + id + "/" + fieldName)
                .build();
    }

    private Object proxyEntity(@NonNull Object entity, @NonNull Map<Method, HttpRequest> relationshipMethods) {

        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(entity.getClass());
        enhancer.setCallback(new RelationshipInterceptor(entity, relationshipMethods));

        return enhancer.create();
    }

    class RelationshipInterceptor implements MethodInterceptor {

        private final Object original;
        private final Map<Method, HttpRequest> relationshipMethods;
        private final Map<Method, Object> valuesCache = new HashMap<>();

        private RelationshipInterceptor(@NonNull Object original, @NonNull Map<Method, HttpRequest> relationshipMethods) {
            this.original = original;
            this.relationshipMethods = relationshipMethods;
        }

        @SuppressWarnings("unchecked")
        public Object intercept(Object object, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {

            Object result = method.invoke(original, args);

            if(result != null || !relationshipMethods.containsKey(method)) {
                return result;
            }

            result = valuesCache.get(method);
            if(result != null) {
                return result;
            }

            HttpRequest request = relationshipMethods.get(method);
            Object relationshipValue = this.findRelationship(request, method.getReturnType());

            valuesCache.put(method, relationshipValue);

            return relationshipValue;
        }

        Object getOriginal() {
            return original;
        }

        @SuppressWarnings("unchecked")
        private Object findRelationship(@NonNull HttpRequest request, @NonNull Class responseType) {

            try {
                return restClient.sendForType(request, responseType);

            } catch (HttpClientErrorException clientException) {

                if (HttpStatus.NOT_FOUND == clientException.getStatusCode()) {
                    return null;
                }
                throw clientException;
            }
        }
    }
}
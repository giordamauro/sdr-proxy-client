package com.mgiorda.sdrproxyclient.impl.resttemplate;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.ser.BeanSerializerModifier;
import com.mgiorda.sdrproxyclient.impl.resttemplate.RelationshipDeserializer.RelationshipInterceptor;
import com.mgiorda.sdrproxyclient.support.RepositoryClientFactory;
import lombok.NonNull;
import org.springframework.util.ReflectionUtils;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class RelationshipSerializer extends JsonSerializer {

    private static final String CGLIB_CLASS_NAME_SUBSTRING = "$$EnhancerByCGLIB$$";
    private static final String CGLIB_CALLBACK_FIELD_NAME = "CGLIB$CALLBACK_0";

    private final String hostUrl;
    private final ObjectMapper jsonMapper;
    private final ReflectionHelper reflectionHelper;
    private final JsonSerializer<Object> defaultSerializer;

    private final PersistenceConfig persistenceConfig;

    RelationshipSerializer(@NonNull String hostUrl, @NonNull ObjectMapper jsonMapper, @NonNull JsonSerializer<Object> defaultSerializer, @NonNull PersistenceConfig persistenceConfig) {

        this.hostUrl = hostUrl;
        this.jsonMapper = jsonMapper;
        this.reflectionHelper = new ReflectionHelper();
        this.defaultSerializer = defaultSerializer;

        this.persistenceConfig = persistenceConfig;
    }

    @Override
    public void serialize(Object object, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {

        Class objClass = object.getClass();

        if (!objClass.isAnnotationPresent(persistenceConfig.getEntityAnnType())) {
            defaultSerializer.serialize(object, jsonGenerator, serializerProvider);

        } else if (objClass.getSimpleName().contains(CGLIB_CLASS_NAME_SUBSTRING)) {

            RelationshipInterceptor interceptor = (RelationshipInterceptor) reflectionHelper.getFieldValue(object, CGLIB_CALLBACK_FIELD_NAME);
            Object original = interceptor.getOriginal();

            ObjectNode jsonNode = jsonMapper.valueToTree(original);
            jsonGenerator.writeRaw(jsonNode.toString());

        } else {

            Map<String, String> relations = new HashMap<>();

            Field[] declaredFields = objClass.getDeclaredFields();
            for (Field field : declaredFields) {

                if (field.isAnnotationPresent(persistenceConfig.getRelationshipAnnType()) && !List.class.isAssignableFrom(field.getType())) {

                    Object fieldValue = reflectionHelper.getFieldValue(object, field);

                    if (fieldValue != null) {

                        Field idField = reflectionHelper.findSingleAnnotatedField(field.getType(), persistenceConfig.getIdAnnTypes());
                        Method getIdMethod = reflectionHelper.getGetterMethod(field.getType(), idField.getName());
                        Object id = ReflectionUtils.invokeMethod(getIdMethod, fieldValue);

                        String relationshipPath = hostUrl + RepositoryClientFactory.getDefaultRepositoryPathFor(field.getType());
                        relations.put(field.getName(), relationshipPath + "/" + id.toString());

                        reflectionHelper.setFieldValue(object, field, null);
                    }
                }
            }

            if(relations.isEmpty()) {
                defaultSerializer.serialize(object, jsonGenerator, serializerProvider);
            }
            else {
                ObjectNode jsonNode = jsonMapper.valueToTree(object);
                relations.forEach(jsonNode::put);

                jsonGenerator.writeRaw(jsonNode.toString());
            }
        }
    }

    public static class Modifier extends BeanSerializerModifier {

        private final String hostUrl;
        private final PersistenceConfig persistenceConfig;
        private final ObjectMapper defaultJsonMapper;

        Modifier(@NonNull String hostUrl, @NonNull PersistenceConfig persistenceConfig, @NonNull ObjectMapper defaultJsonMapper) {
            this.hostUrl = hostUrl;
            this.persistenceConfig = persistenceConfig;
            this.defaultJsonMapper = defaultJsonMapper;
        }

        @Override
        @SuppressWarnings("unchecked")
        public JsonSerializer<?> modifySerializer(SerializationConfig config, BeanDescription beanDesc, JsonSerializer<?> serializer) {

            return new RelationshipSerializer(hostUrl, defaultJsonMapper, (JsonSerializer<Object>) serializer, persistenceConfig);
        }
    }
}
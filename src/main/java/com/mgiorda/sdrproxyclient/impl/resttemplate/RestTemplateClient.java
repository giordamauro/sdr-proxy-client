package com.mgiorda.sdrproxyclient.impl.resttemplate;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.mgiorda.sdrproxyclient.model.HttpRequest;
import com.mgiorda.sdrproxyclient.model.RestClient;
import lombok.NonNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.hateoas.hal.Jackson2HalModule;
import org.springframework.http.*;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class RestTemplateClient implements RestClient {

    private final String hostUrl;

    private final ObjectMapper jsonMapper;
    private final RestTemplate restTemplate;

    public RestTemplateClient(@NonNull String hostUrl, @NonNull PersistenceConfig persistenceConfig, @NonNull ClientConfig clientConfig) {

        this.hostUrl = hostUrl;

        ObjectMapper defaultJsonMapper = new ObjectMapper();
        defaultJsonMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        defaultJsonMapper.registerModule(new Jackson2HalModule());

        SimpleModule halModule = new Jackson2HalModule()
                .setSerializerModifier(new RelationshipSerializer.Modifier(hostUrl, persistenceConfig, defaultJsonMapper));
        if (clientConfig.getProxyRelationships()) {
            halModule.setDeserializerModifier(new RelationshipDeserializer.Modifier(this, persistenceConfig, defaultJsonMapper));
        }

        jsonMapper = new ObjectMapper();
        jsonMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        jsonMapper.registerModule(halModule);

        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setSupportedMediaTypes(MediaType.parseMediaTypes("application/hal+json"));
        converter.setObjectMapper(jsonMapper);

        this.restTemplate = new RestTemplate(Collections.singletonList(converter));

        if(clientConfig.getJsonErrorHandler()) {
            JsonErrorHandler errorHandler = new JsonErrorHandler(defaultJsonMapper);
            restTemplate.setErrorHandler(errorHandler);
        }
    }

    @Override
    public <T> void send(@NonNull HttpRequest<T> request) {

        String serviceUrl = this.getServiceQueryUrl(request);
        HttpEntity<T> requestEntity = this.getRequestEntity(request);

        restTemplate.exchange(serviceUrl, request.getHttpMethod(), requestEntity, Object.class);
    }

    @Override
    public <T, E> E sendForType(@NonNull HttpRequest<T> request, @NonNull Class<E> responseType) {

        String serviceUrl = this.getServiceQueryUrl(request);
        HttpEntity<T> requestEntity = this.getRequestEntity(request);

        ResponseEntity<E> responseEntity = restTemplate.exchange(serviceUrl, request.getHttpMethod(), requestEntity,
                responseType);

        return responseEntity.getBody();
    }

    @Override
    public <E> List<E> sendForList(@NonNull HttpRequest request, @NonNull Class<E> responseType) {

        String serviceUrl = this.getServiceQueryUrl(request);
        HttpEntity<?> requestEntity = this.getRequestEntity(request);

        ResponseEntity<PageResponse> responseEntity = restTemplate.exchange(serviceUrl, request.getHttpMethod(), requestEntity,
                PageResponse.class);

        Map<String, List<JsonNode>> embedded = responseEntity.getBody().get_embedded();
        List<JsonNode> resources = embedded.values().iterator().next();

        return resources.stream()
                .map(resource -> this.readJsonNode(resource, responseType))
                .collect(Collectors.toList());
    }

    @Override
    public <E> Page<E> sendForPage(@NonNull HttpRequest request, @NonNull Class<E> responseType) {

        String serviceUrl = this.getServiceQueryUrl(request);
        HttpEntity<?> requestEntity = this.getRequestEntity(request);

        ResponseEntity<PageResponse> responseEntity = restTemplate.exchange(serviceUrl, request.getHttpMethod(), requestEntity,
                PageResponse.class);

        PageResponse pageResponse = responseEntity.getBody();
        Map<String, List<JsonNode>> embedded = pageResponse.get_embedded();
        List<JsonNode> resources = embedded.values().iterator().next();
        PageResponse.Page page = pageResponse.getPage();

        List<E> responseList = resources.stream()
                .map(resource -> this.readJsonNode(resource, responseType))
                .collect(Collectors.toList());

        return new PageImpl<E>(responseList, new PageRequest(page.getNumber(), page.getSize()), page.getTotalElements());
    }

    @SuppressWarnings("unchecked")
    private String getServiceQueryUrl(@NonNull HttpRequest request) {

        String serviceUrl = hostUrl + request.getServicePath();

        Map<String, List<String>> queryParams = (Map<String, List<String>>) request.getQueryParams().orElse(null);
        if(queryParams != null) {

            String queryString = queryParams.entrySet()
                    .stream()
                    .map(entry -> entry.getValue()
                            .stream()
                            .map(value -> entry.getKey() + "=" + value)
                            .collect(Collectors.joining("&"))
                    )
                    .collect(Collectors.joining("&"));

            serviceUrl += "?" + queryString;
        }

        return serviceUrl;
    }

    private <E> HttpEntity<E> getRequestEntity(@NonNull HttpRequest<E> request) {

        HttpEntity<E> requestEntity = null;

        HttpHeaders headers = request.getHeaders()
                .map(requestHeaders -> {

                    HttpHeaders httpHeaders = new HttpHeaders();
                    requestHeaders.forEach(httpHeaders::add);

                    return httpHeaders;
                })
                .orElse(null);

        E requestBody = request.getRequestBody()
                .orElse(null);

        if(headers != null || requestBody != null) {
            requestEntity = new HttpEntity<E>(requestBody, headers);
        }

        return requestEntity;
    }

    private <E> E readJsonNode(@NonNull JsonNode jsonNode, @NonNull Class<E> responseType) {

        try {
            return jsonMapper.readValue(jsonNode.toString(), responseType);
        }
        catch (Exception e) {
            throw new IllegalStateException("Exception reading JsonNode" , e);
        }
    }
}

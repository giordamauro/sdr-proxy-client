package com.mgiorda.sdrproxyclient.model;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import org.springframework.http.HttpMethod;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Data
@Builder
public class HttpRequest<T> {

    @NonNull
    private final HttpMethod httpMethod;

    @NonNull
    private final String servicePath;

    private final Map<String, String> headers;

    private final Map<String, List<String>> queryParams;

    private final T requestBody;

    public Optional<Map<String, String>> getHeaders() {
        return Optional.ofNullable(headers);
    }

    public Optional<Map<String, List<String>>> getQueryParams() {
        return Optional.ofNullable(queryParams);
    }

    public Optional<T> getRequestBody() {
        return Optional.ofNullable(requestBody);
    }

}

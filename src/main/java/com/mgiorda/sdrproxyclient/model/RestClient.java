package com.mgiorda.sdrproxyclient.model;

import org.springframework.data.domain.Page;

import java.util.List;

public interface RestClient {

    <T> void send(HttpRequest<T> request);

    <T, E> E sendForType(HttpRequest<T> request, Class<E> responseType);

    <E>  List<E> sendForList(HttpRequest request, Class<E> responseType);

    <E> Page<E> sendForPage(HttpRequest request, Class<E> responseType);
}

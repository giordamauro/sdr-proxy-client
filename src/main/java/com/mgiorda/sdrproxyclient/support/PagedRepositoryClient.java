package com.mgiorda.sdrproxyclient.support;

import com.mgiorda.sdrproxyclient.model.HttpRequest;
import com.mgiorda.sdrproxyclient.model.RestClient;
import lombok.NonNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.http.HttpMethod;

import java.io.Serializable;
import java.util.*;

class PagedRepositoryClient<T, ID extends Serializable> implements PagingAndSortingRepository<T, ID> {

    final RestClient restClient;
    final String repositoryName;
    final Class<T> entityType;

    final Map<String, String> headers = new HashMap<>();

    PagedRepositoryClient(@NonNull RestClient restClient, @NonNull String repositoryName, @NonNull Class<T> entityType) {

        this.restClient = restClient;
        this.repositoryName = repositoryName;
        this.entityType = entityType;
    }

    public void setHeader(@NonNull String headerName, @NonNull Object value) {
        headers.put(headerName, String.valueOf(value));
    }

    @Override
    @SuppressWarnings("unchecked")
    public T findOne(@NonNull ID id) {

        HttpRequest request = HttpRequest.builder()
                .httpMethod(HttpMethod.GET)
                .servicePath("/" + repositoryName + "/" + id.toString())
                .headers(headers)
                .build();

        return (T) restClient.sendForType(request, entityType);
    }

    @Override
    public Iterable<T> findAll() {

        HttpRequest request = HttpRequest.builder()
                .httpMethod(HttpMethod.GET)
                .servicePath("/" + repositoryName)
                .headers(headers)
                .build();

        return restClient.sendForList(request, entityType);
    }

    @Override
    public Iterable<T> findAll(@NonNull Sort sort) {

        HttpRequest request = HttpRequest.builder()
                .httpMethod(HttpMethod.GET)
                .servicePath("/" + repositoryName + "/sort=" + sort.toString())
                .headers(headers)
                .build();

        return restClient.sendForList(request, entityType);
    }

    @Override
    public Page<T> findAll(@NonNull Pageable pageable) {

        Map<String, List<String>> queryParams = this.getPageableQueryParams(pageable);

        HttpRequest request = HttpRequest.builder()
                .httpMethod(HttpMethod.GET)
                .servicePath("/" + repositoryName)
                .queryParams(queryParams)
                .headers(headers)
                .build();

        return restClient.sendForPage(request, entityType);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void delete(@NonNull ID id) {

        HttpRequest request = HttpRequest.builder()
                .httpMethod(HttpMethod.DELETE)
                .servicePath("/" + repositoryName + "/" + id.toString())
                .headers(headers)
                .build();

        restClient.send(request);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void deleteAll() {

        HttpRequest request = HttpRequest.builder()
                .httpMethod(HttpMethod.DELETE)
                .servicePath("/" + repositoryName)
                .headers(headers)
                .build();

        restClient.send(request);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <S extends T> S save(S entity) {
        throw new UnsupportedOperationException("Not supported yet - Use 'create' or 'update' methods");
    }

    @Override
    public Iterable<T> findAll(Iterable<ID> iterable) {
        throw new UnsupportedOperationException("Not supported - Use findOne for each element");
    }

    @Override
    public <S extends T> Iterable<S> save(Iterable<S> iterable) {
        throw new UnsupportedOperationException("Not supported - Use Create or Update for each element");
    }

    @Override
    public boolean exists(ID id) {
        throw new UnsupportedOperationException("Not supported - Use findOne");
    }

    @Override
    public long count() {
        throw new UnsupportedOperationException("Not supported - Use findAll Page.getTotalElements");
    }

    @Override
    public void delete(T t) {
        throw new UnsupportedOperationException("Not supported - Use delete by Id");
    }

    @Override
    public void delete(Iterable<? extends T> iterable) {
        throw new UnsupportedOperationException("Not supported yet");
    }

    Map<String, List<String>> getPageableQueryParams(@NonNull Pageable pageable) {

        Map<String, List<String>> queryParams = new LinkedHashMap<>();

        int pageNumber = pageable.getPageNumber();
        int pageSize = pageable.getPageSize();
        Sort sort = pageable.getSort();

        queryParams.put("page", Collections.singletonList(String.valueOf(pageNumber)));
        queryParams.put("size", Collections.singletonList(String.valueOf(pageSize)));

        if (sort != null) {
            queryParams.put("sort", Collections.singletonList(sort.toString()));
        }

        return queryParams;
    }
}
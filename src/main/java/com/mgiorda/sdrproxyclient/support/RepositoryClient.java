package com.mgiorda.sdrproxyclient.support;

import lombok.NonNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.Repository;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class RepositoryClient<T extends Repository<S, ID>, S, ID extends Serializable> {

    private final T proxyClient;
    private final SupportRepositoryClient<S, ID> repositoryClient;

    RepositoryClient(@NonNull T proxyClient, @NonNull SupportRepositoryClient<S, ID> repositoryClient) {

        this.proxyClient = proxyClient;
        this.repositoryClient = repositoryClient;
    }

    public RepositoryClient<T, S, ID> setHeader(@NonNull String headerName, @NonNull Object value) {
        repositoryClient.setHeader(headerName, value);

        return this;
    }

    public T proxy() {
        return proxyClient;
    }

    public S create(S entity) {
        return repositoryClient.create(entity);
    }

    public <E extends S> E update(ID id, E entity) {
        return repositoryClient.update(id, entity);
    }

    public List<S> findBy(Map<String, Object> queryFields) {
        return repositoryClient.findByQueryFields(queryFields);
    }

    public Page<S> findPageBy(Map<String, Object> queryFields, Pageable pageable) {
        return repositoryClient.findPageByQueryFields(queryFields, pageable);
    }

    //TODO Move this funtionality to CGLIB
    public <E> E getRelationship(ID id, String fieldName, Class<E> relatedEntityType) {
        return repositoryClient.findOneByRelationship(id, fieldName, relatedEntityType);
    }

    public <E> E getRelationship(@NonNull ID id, @NonNull Class<E> relatedEntityType) {

        String entityName = relatedEntityType.getSimpleName();
        String defaultRelationshipName = Character.toLowerCase(entityName.charAt(0)) + entityName.substring(1);

        return repositoryClient.findOneByRelationship(id, defaultRelationshipName, relatedEntityType);
    }
}
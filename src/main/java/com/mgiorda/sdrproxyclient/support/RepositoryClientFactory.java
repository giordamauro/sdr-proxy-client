package com.mgiorda.sdrproxyclient.support;

import com.mgiorda.sdrproxyclient.model.RestClient;
import lombok.NonNull;
import org.atteo.evo.inflector.English;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.Repository;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Proxy;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public class RepositoryClientFactory {

    private final Map<Class<?>, Object> proxyObjects = new HashMap<>();
    private final RestClient restClient;

    public RepositoryClientFactory(@NonNull RestClient restClient) {

        this.restClient = restClient;
    }

    public <T extends Repository<S, ID>, S, ID extends Serializable> RepositoryClient<T, S, ID> getClient(@NonNull Class<T> repositoryInterface) {

        String repositoryPath = this.getRepositoryPath(repositoryInterface);
        Class<S> entityType = this.getRepositoryEntityType(repositoryInterface);

        SupportRepositoryClient<S, ID> repositoryClient = new SupportRepositoryClient<>(restClient, repositoryPath, entityType);
        RepositoryInvocationHandler<S, ID> invocationHandler = new RepositoryInvocationHandler<>(repositoryClient);

        Object proxyObject = proxyObjects.computeIfAbsent(repositoryInterface, interfaceClass -> {

            ClassLoader classLoader = interfaceClass.getClassLoader();
            Class<?>[] implClass = new Class<?>[]{interfaceClass};

            return Proxy.newProxyInstance(classLoader, implClass, invocationHandler);
        });

        @SuppressWarnings("unchecked")
        T proxy = (T) proxyObject;

        return new RepositoryClient<>(proxy, repositoryClient);
    }

    private <T extends Repository<S, ID>, S, ID extends Serializable> String getRepositoryPath(@NonNull Class<T> repositoryInterface) {

        Class<S> entityType = this.getRepositoryEntityType(repositoryInterface);

        return getDefaultRepositoryPathFor(entityType);
    }

    @SuppressWarnings("unchecked")
    private <T extends Repository<S, ID>, S, ID extends Serializable> Class<S> getRepositoryEntityType(@NonNull Class<T> repositoryInterface) {

        if (!repositoryInterface.isInterface()) {
            throw new IllegalArgumentException("Repository class is not an interface");
        }

        Type dataInterface = this.findGenericInterface(repositoryInterface);
        Type entityType = ((ParameterizedType) dataInterface).getActualTypeArguments()[0];

        return (Class<S>) entityType;
    }

    private <T extends Repository<S, ID>, S, ID extends Serializable> Type findGenericInterface(@NonNull Class<T> repositoryInterface) {

        Type[] genericInterfaces = repositoryInterface.getGenericInterfaces();

        for (Type genericInterface : genericInterfaces) {

            ParameterizedType paramType = (ParameterizedType) genericInterface;
            Class<?> interfaceClass = (Class<?>) paramType.getRawType();

            if (PagingAndSortingRepository.class.isAssignableFrom(interfaceClass)) {
                return genericInterface;
            } else if (CrudRepository.class.isAssignableFrom(interfaceClass)) {
                return genericInterface;
            } else if (Repository.class.isAssignableFrom(interfaceClass)) {
                return genericInterface;
            }
        }

        throw new IllegalStateException("Couldn't find a generic interface for Repository interface: " + repositoryInterface);
    }

    public static String getDefaultRepositoryPathFor(@NonNull Class<?> type) {

        String simpleTypeName = StringUtils.uncapitalize(type.getSimpleName());

        return English.plural(simpleTypeName);
    }
}
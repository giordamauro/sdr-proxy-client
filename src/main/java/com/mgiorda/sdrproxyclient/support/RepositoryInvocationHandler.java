package com.mgiorda.sdrproxyclient.support;

import lombok.NonNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.io.Serializable;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.*;
import java.util.stream.Collectors;

class RepositoryInvocationHandler<T, ID extends Serializable> implements InvocationHandler {

    private final SupportRepositoryClient<T, ID> repositoryClient;

    RepositoryInvocationHandler(@NonNull SupportRepositoryClient<T, ID> repositoryClient) {

        this.repositoryClient = repositoryClient;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        try {
            Class<?> declaringClass = method.getDeclaringClass();
            if (CrudRepository.class.equals(declaringClass) || PagingAndSortingRepository.class.equals(declaringClass)) {

                return method.invoke(repositoryClient, args);

            } else if(method.getName().contains("find")){

                return this.sendSearchRequest(method, args);
            }
    }
        catch (Exception e) {
            throw new IllegalStateException("Exception invoking RepositoryClient proxy method: " + method.getName() , e);
        }

        throw new IllegalStateException("Couldn't recognize method: " + method.getName());
    }

    private Object sendSearchRequest(@NonNull Method method, Object[] args) {

        String searchName = method.getName();
        Class<?> returnType = method.getReturnType();

        Map<String, List<String>> queryFields = this.getQueryFields(method, args);
        Pageable pageable = this.findPageableArgument(args);

        if(searchName.contains("findBy")){

            if(Page.class.isAssignableFrom(returnType)) {

                if(pageable == null) {
                    throw new IllegalStateException("Page expected but no Pageable parameter");
                }
                return repositoryClient.searchByQueryFields(searchName, queryFields, pageable);
            }
            else {
                return repositoryClient.searchByQueryFields(searchName, queryFields);
            }
        }
        else if(searchName.contains("findOneBy")){

            Object result = repositoryClient.findOneByQueryFields(searchName, queryFields);
            return (Optional.class.equals(returnType)) ? Optional.ofNullable(result) : result;
        }

        throw new IllegalStateException("Couldn't recognize method: " + searchName);
    }

    private Pageable findPageableArgument(@NonNull Object[] args) {

        for (Object argValue : args) {

            if (Pageable.class.isAssignableFrom(argValue.getClass())) {
                return (Pageable) argValue;
            }
        }
        return null;
    }

    private Map<String, List<String>> getQueryFields(@NonNull Method method, Object[] args) {

        List<String> parameterNames = this.getParameterNames(method);

        Map<String, List<String>> queryFields = new HashMap<>();
        for (int i = 0; i < args.length; i++) {

            Object argValue = args[i];

            if(!Pageable.class.isAssignableFrom(argValue.getClass())) {

                String parameterName = parameterNames.get(i);
                List<String> values = queryFields.computeIfAbsent(parameterName, key -> new ArrayList<>());

                values.add(String.valueOf(argValue));
            }
        }

        return queryFields;
    }

    private List<String> getParameterNames(@NonNull Method method) {

        Parameter[] parameters = method.getParameters();

        return Arrays.stream(parameters)
                .filter(parameter -> !Pageable.class.isAssignableFrom(parameter.getType()))
                .map(this::getParameterName)
                .collect(Collectors.toList());
    }

    private String getParameterName(@NonNull Parameter parameter) {

        Param annotation = parameter.getDeclaredAnnotation(Param.class);
        if (annotation != null) {
            return annotation.value();
        }

        if (!parameter.isNamePresent()) {
            throw new IllegalStateException("Parameter names are not present! Enable by compiler argument");
        }

        return parameter.getName();
    }
}

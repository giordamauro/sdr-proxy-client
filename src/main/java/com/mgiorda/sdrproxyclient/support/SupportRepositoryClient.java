package com.mgiorda.sdrproxyclient.support;

import com.mgiorda.sdrproxyclient.model.HttpRequest;
import com.mgiorda.sdrproxyclient.model.RestClient;
import lombok.NonNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

class SupportRepositoryClient<T, ID extends Serializable> extends PagedRepositoryClient<T, ID> {

    SupportRepositoryClient(@NonNull RestClient restClient, @NonNull String repositoryName, @NonNull Class<T> entityType) {
        super(restClient, repositoryName, entityType);
    }

    @SuppressWarnings("unchecked")
    public <S extends T> S create(@NonNull S entity) {

        HttpRequest request = HttpRequest.builder()
                .httpMethod(HttpMethod.POST)
                .servicePath("/" + repositoryName)
                .requestBody(entity)
                .headers(headers)
                .build();

        return (S) restClient.sendForType(request, entityType);
    }

    @SuppressWarnings("unchecked")
    public <S extends T> S update(@NonNull ID id, @NonNull S entity) {

        HttpRequest request = HttpRequest.builder()
                .httpMethod(HttpMethod.PUT)
                .servicePath("/" + repositoryName + "/" + id.toString())
                .requestBody(entity)
                .headers(headers)
                .build();

        return (S) restClient.sendForType(request, entityType);
    }

    public Iterable<T> searchByQueryFields(@NonNull String searchName, @NonNull Map<String, List<String>> queryFields) {

        HttpRequest request = HttpRequest.builder()
                .httpMethod(HttpMethod.GET)
                .queryParams(queryFields)
                .servicePath("/" + repositoryName + "/search/" + searchName)
                .headers(headers)
                .build();

        return restClient.sendForList(request, entityType);
    }

    public Page<T> searchByQueryFields(@NonNull String searchName, @NonNull Map<String, List<String>> queryFields, @NonNull Pageable pageable) {

        Map<String, List<String>> pageableQueryParams = this.getPageableQueryParams(pageable);

        Map<String, List<String>> queryParams = new LinkedHashMap<>();
        queryParams.putAll(queryFields);
        queryParams.putAll(pageableQueryParams);

        HttpRequest request = HttpRequest.builder()
                .httpMethod(HttpMethod.GET)
                .queryParams(queryParams)
                .servicePath("/" + repositoryName + "/search/" + searchName)
                .headers(headers)
                .build();

        return restClient.sendForPage(request, entityType);
    }

    public List<T> findByQueryFields(@NonNull Map<String, Object> queryFields) {

        Map<String, List<String>> queryParams = this.getFieldsQueryParams(queryFields);

        HttpRequest request = HttpRequest.builder()
                .httpMethod(HttpMethod.GET)
                .queryParams(queryParams)
                .servicePath("/" + repositoryName)
                .headers(headers)
                .build();

        return restClient.sendForList(request, entityType);
    }

    public Page<T> findPageByQueryFields(@NonNull Map<String, Object> queryFields, @NonNull Pageable pageable) {

        Map<String, List<String>> pageableQueryParams = this.getPageableQueryParams(pageable);
        Map<String, List<String>> queryFieldValues = this.getFieldsQueryParams(queryFields);

        Map<String, List<String>> queryParams = new LinkedHashMap<>();
        queryParams.putAll(queryFieldValues);
        queryParams.putAll(pageableQueryParams);

        HttpRequest request = HttpRequest.builder()
                .httpMethod(HttpMethod.GET)
                .queryParams(queryParams)
                .servicePath("/" + repositoryName)
                .headers(headers)
                .build();

        return restClient.sendForPage(request, entityType);
    }

    @SuppressWarnings("unchecked")
    public T findOneByQueryFields(@NonNull String searchName, @NonNull Map<String, List<String>> queryFields) {

        HttpRequest request = HttpRequest.builder()
                .httpMethod(HttpMethod.GET)
                .queryParams(queryFields)
                .servicePath("/" + repositoryName + "/search/" + searchName)
                .headers(headers)
                .build();

        try {
            return (T) restClient.sendForType(request, entityType);
        }
        catch(HttpClientErrorException clientException) {

            if(HttpStatus.NOT_FOUND == clientException.getStatusCode()) {
                return null;
            }
            throw clientException;
        }
    }

    @SuppressWarnings("unchecked")
    public <E> E findOneByRelationship(@NonNull ID id, @NonNull String fieldName, @NonNull Class<E> relatedEntityType) {

        HttpRequest request = HttpRequest.builder()
                .httpMethod(HttpMethod.GET)
                .servicePath("/" + repositoryName + "/" + id + "/" + fieldName)
                .headers(headers)
                .build();

        return (E) restClient.sendForType(request, relatedEntityType);
    }

    private Map<String, List<String>> getFieldsQueryParams(@NonNull Map<String, Object> queryFields) {

        Map<String, List<String>> queryParams = new LinkedHashMap<>();

        for (Map.Entry<String, Object> fieldEntry : queryFields.entrySet()) {

            Object fieldValue = fieldEntry.getValue();
            List<String> queryFieldValues = this.getQueryValuesForField(fieldValue);

            queryParams.put(fieldEntry.getKey(), queryFieldValues);
        }

        return queryParams;
    }

    @SuppressWarnings("unchecked")
    private List<String> getQueryValuesForField(@NonNull Object fieldValue) {

        List<String> values = new ArrayList<>();

        if (!List.class.isAssignableFrom(fieldValue.getClass())) {
            values.add(String.valueOf(fieldValue));
        }
        else {
            List<Object> multiValued = (List<Object>) fieldValue;
            multiValued.forEach(object -> values.add(String.valueOf(object)));
        }

        return values;
    }
}